let JsoqsDb = require("jsoqs-db");
let testAdapter = require("jsoqs-db/test.js");
let {Neo4jAdapter} = require("./adapt.js");
let faker = require("faker");
let assert = require("assert");
const Lab = require('lab');
const { after, before, describe, it } = exports.lab = Lab.script();

async function test() {
    await testAdapter({
        Adapter: await new Neo4jAdapter({
            host: "localhost",
            port: 7687,
            user: "neo4j",
            password: "newPassword"
        }),
        JsoqsDb,
        describe,
        it,
        assert,
        faker
    })
}

test();