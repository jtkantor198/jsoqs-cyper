let AbstractAdapter = require("./abstractAdapter.js");

class CyperQueryTransformer {
    constructor({lookup, database, items, type}){
        this.match = [];
        this.matchWhere = [];
        this.create = [];
        this.jsoqDb = database;
        this.lookup = lookup;
        this.items = items;
        this.type = type;
        console.log(lookup)
        if (!(lookup.path[0]&&lookup.path[0].collection)){
            throw Error("Jsoq must have target collection")
        }
        this.baseCollection = this.jsoqDb.getCollectionByName(lookup.path[0].collection).model;
        this.labels = {};
        this.nextLabel = (collection)=>{
            if (this.labels[collection]===undefined){
                this.labels[collection] = 0;
            }
            else{
                this.labels[collection]++;
            }
            return collection+this.labels[collection];
        }
        this.generateAttributes = function(attributes, item){
            let attrs = "";
            for (let attr of Object.keys(attributes)){
                if (item[attr]!==undefined){
                    attrs+=attr+":"+this.toCyper(item[attr], attributes[attr].type)
                }
            }
            return attrs;
        }
        this.generateAssociations = function(collection, item){
            let associations = collection.associations;
            for (let asso of Object.keys(associations)){
                if (item[asso]!==undefined){
                    if (item[asso] instanceof Array){
                        for (let item of item[asso]){
                            this.generateAssociations(associations, item)
                        }
                    }
                    else{
                        this.generateAssociations(associations, item)
                    }
                    attrs+=attr+":"+this.toCyper(item[attr], attributes[attr].type)
                }
            }
            return attrs;
        }
        this.toCyper = function(value, type){
            if (type==="string"){
                return '"'+value.replace(/"/g,'\"')+'"'
            }
            else if (type==="number"){
                return value;
            }
            else{
                throw Error(`Type '${type}' note recognized.`)
            }
        }
    };
    matchDescent(){
        let attributes =  this.baseCollection.attributes;
        let associations = this.baseCollection.associations;
        let restPath = this.lookup.path.slice(1,Infinity);
        let match = `(${this.nextLabel(this.lookup.path[0].collection)}:${this.lookup.path[0].collection})`;
        console.log(this)
        if (this.type==="insert"){
            //If targeting collection, just get last one
            let itemCollection = this.lookup.path[0].collection;
            for (let item of restPath){
                if (associations[item.name]){
                    itemCollection = associations[item.name].collection;
                    if (item.filters){
                        break; //must do match
                    }
                }
                else if (attributes[item.name]){
                    throw Error(`Attribute '${item.name}' of '${collection}' is not an association.\nAn insert must target a collection.`);
                }
                else{
                    throw Error(`Attribute '${item.name}' of '${collection}' not found`);
                }
            }
            this.insertTargetCollection = this.jsoqDb.getCollectionByName(itemCollection).model
            console.log(itemCollection)
            return itemCollection;
            //If targeting node, match
        }
        
            for (let item of restPath){
                if (associations[item.name]){
                    let itemCollection = associations[item.name].collection;
                    item._jsoq_adapter_label = `${this.nextLabel(itemCollection)}`;
                    match+=`->[:${index}]->(${item._jsoq_adapter_label}:${itemCollection})`;
                    if (item.filters){
                        //generate where condition

                        this.matchWhere.push(item.filters)
                    }
                }
                else if (attributes[item.name]){
                    throw Error(`Attribute '${item.name}' of '${collection}' is not an association`)
                }
                else{
                    throw Error(`Attribute '${item.name}' of '${collection}' not found`)
                }
            }
            if (match){
                this.match.push(match);
            }
            for (let item of restPath){
                if (associations[item.name]){
                    let itemCollection = associations[item.name].collection;
                    item._jsoq_adapter_label = `${this.nextLabel(itemCollection)}`;
                    match+=`->[:${index}]->(${item._jsoq_adapter_label}:${itemCollection})`;
                    if (item.filters){
                        //generate where condition

                        this.matchWhere.push(item.filters)
                    }
                }
                else if (attributes[item.name]){
                    throw Error(`Attribute '${item.name}' of '${collection}' is not an association`)
                }
                else{
                    throw Error(`Attribute '${item.name}' of '${collection}' not found`)
                }
            }
            if (match){
                this.match.push(match);
            }
            console.log(this.match, this.matchWhere)
        
    }
    matchDesire(){
        let fields = this.lookup.fields;
        for (let field of fields){
            if (associations[item.name]){
                let itemCollection = associations[item.name].collection;
                match+=`->[:${index}]->(${item.label}:${itemCollection})`;
                if (item.fields){
                    for (let path of item.fields){
                        match+="\n"+createMatchClauseAndAttachLabelsToPath(itemCollection, database, path, baseItems);
                    }
                }
            }
            else if (attributes[item.name]){
                throw Error(`Attribute '${item.name}' of '${collection}' is not an association`)
            }
            else{
                throw Error(`Attribute '${item.name}' of '${collection}' not found`)
            }
        }
    }
    setValuesAndRelations(){
        //set/create -> values and relations
    }
    applyFilteringPipes(){}
    applyAggregationPipes(){}
    createNodesAndRelationships(){
        let attributes = this.insertTargetCollection.attributes;
        if (this.type==="insert"){
            for (let item of this.items){
                let attrs = this.generateAttributes(attributes, item)
                this.create.push(`(${this.nextLabel(this.insertTargetCollection.name)}:${this.insertTargetCollection.name} {${attrs}})`)
                this.generateAssociations(this.insertTargetCollection, item)
            }
        }
    }
    deleteNodes(){}
    generateQuery(){
        let query = this.match.map(str=>'MATCH '+str).join("\n")
            +this.matchWhere.map(str=>'WHERE '+str).join("\n")
            +this.create.map(str=>"CREATE "+str).join("\n")
        console.log(query);
        return query;
    }
}

class Neo4jQueryRunner{
    constructor(settings){
        const neo4j = require('neo4j-driver').v1;
        const driver = neo4j.driver(`bolt://${settings.host}:${settings.port}`, neo4j.auth.basic(settings.user, settings.password));
        this.db = driver.session()
    }
    async ensureCollection(collection){
        let q = "";
        let attrs = collection.model.attributes;
        for (let attr of Object.keys(attrs)){
            q+=`CREATE INDEX ON :${collection.name}(attr)`
        }
        return await this.runQuery(q)
    }
    runQuery(query){

    }
}

class Neo4jResultTransformer{

}

class Neo4jAdapter extends AbstractAdapter{
    constructor(settings){
        super({
            queryTransformer: CyperQueryTransformer,
            queryRunner: Neo4jQueryRunner,
            queryResultTransformer: Neo4jResultTransformer,
            settings
        })
    }
}

module.exports = {
    Neo4jAdapter
};
