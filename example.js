const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver("bolt://localhost:7687", neo4j.auth.basic("neo4j", "newPassword"));

const session = driver.session();

const cypher = "MATCH (p:Person {name: {name} }) RETURN count(p) AS count";
const params = { name: "Adam" };

async function run() {
    console.log(await session.run(`
    CREATE (p:Person { name: "adam" })
    CREATE (d:Dog {name: "doggy"})
    CREATE (p)-[r:pets]->(d)
    return p, d
    `));
    console.log(await session.run(`CREATE (p:Person { name: "adam" });`));
    console.log((await session.run(`MATCH (p:Person)-[:pets]->(d:Dog) RETURN p{ .name, d: collect(d { .name })}`)).records.map((rec)=>JSON.stringify(rec.toObject())));
}

run().then(()=>{
    driver.close();
}).catch((err)=>{
    console.log(err);
})


