
let implement = require("../implement")
let ConditionTransformer = require("jsoqs-db/jsoqFiltersToQueryLang.js")
let conditionTransformer = new ConditionTransformer()
conditionTransformer.addOrFunction((items)=>{

})
conditionTransformer.addAndFunction((items)=>{
    
})
conditionTransformer.addConditionFunction((condition)=>{
    
})
//conditions : ">" / "<" / "=" / ":" / ">=" / "<=" / "==" / "===" / "~" / "#" / "{" /  "!=" / "!~" / "!{";
//pipes
//aggregations
class CyperAdapter{
    /**
     * Creates database connection
     * @param {*} settings 
     */
    constructor(settings){
        implement(settings, {
            host: "string",
            port: "number",
            user: "string",
            password: "string"
        })
        const neo4j = require('neo4j-driver').v1;
        const driver = neo4j.driver(`bolt://${settings.host}:${settings.port}`, neo4j.auth.basic(settings.user, settings.password));
        this.db = driver.session();
    }
    /**
     * Ensures collections are implemented as schema
     * @param {*} collection 
     */
    async ensureCollection(collection){
        /*implement(collection, {
            model: {
                name: 'string',
                attributes: {
                    '[any]': {type: 'string'} 
                },
                associations: {
                    '[any]': {
                        inverse: 'string',
                        collection: 'string',
                        type: 'oneToOne,oneToMany,manyToOne,manyToMany'
                    }
                }
            }
        })*/
        let q = "";
        let attrs = collection.model.attributes;
        for (let attr of Object.keys(attrs)){
            q+=`CREATE INDEX ON :${collection.name}(attr)`
        }

    }
    async find(lookup, database){
        console.log(lookup);
        process.exit();
        let query = '';
        //Need a match clause to describe descent structure
        query+="MATCH (p:Person)-[:pets]->(d:Dog)"
        for (let item of lookup){
            let associationName = item.name;
            let association = lastCollection.model.associations[associationName];
            let collection = database.getCollectionByName(association.collection);
            `(${item.name}:${item.name})-[:${database.getAssociationIndexName(lastCollection.model.name, associationName, association)}]->`
            lastCollection = collection;
        }
        //Need a where clause for each condition
        //descent: dsaad[dsd=sda].sdfsf[sdd=2]
        //desire: {people[name=thing]{name, other}}

        //In neo4j the match is the name regardless of whether it is decent or desire, just need to change the desire
        for (let item of lookup){
            `WHERE `
        }
        //Need a 
    }
    getFromInsertTarget(lookup){
        let match, targetCollection;
        let target = lookup.path[lookup.path.length-1];
        targetCollection = path[0].collection;
        if (lookup.path.length > 1){

        }
        else{
            let targetNodesCol = lookup.path[lookup.path.length-2];
            let match = `MATCH (targetNodes: ${targetNodesCol})`;
            match+= `\nWHERE (targetNodes: ${targetNodesCol})${where}`
            //need to climb up lookup ensuring propery/relation complience

        }
    }
    matchDescent(path){
        let revPath = [...path].reverse();
        let match = `MATCH (targetNodes: ${targetNodesCol})`;
        for (let item of revPath){
            match+=`->[:${associationIndex}]->(${revPath.collection}_path_target: ${revPath.collection})`
        }
        return match;
    }
    generateWhereCondition(path){
        //just need to create properly and/or'd list of conditions
        let revPath = [...path].reverse();
        let where=""
        for (let item of revPath){
            where+=`->[:${revPath.collection}]->()`
            //Need to handle conditions for each comparison type (>,=,<,:,>=,<=,in,matches regex?)
            //Need to hanlde and/or
            /*
             MATCH (targetNode: Col1)->[relName1]]->(col2itm: relName2)
             WHERE col2itm.name="aname" || col2item.age>20 
             */
        }
        return where;
    }
    comparisonToCyper(condition){

    }
    /**
     * Inserts objects into a collection or list
     * @param {*} lookup 
     * @param {*} item 
     */
    async insert(lookup, item, database){
        console.log(lookup, item)
        let {items, relations} = this.getItemsAndRelations({collection: lookup.path[lookup.path.length-1].collection, item: item, database: database})
        //create all nodes
        //relate all nodes
        
        //insert subset will invole finding next to last node descent and creating relations to those nodes
    }
    toCyperByType(value){
        if (typeof value === "string"){
            function addslashes( str ) { return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0'); }
            return `"${addslashes(value)}"`
        }
        else if (typeof value === "number"){
            return value;
        }
    }
    generateCyperInsert(match, items, relations){
        function itemsToCyper(item, collection){
            let attrs = [];
            let _jsoq_adap_neoName = item._jsoq_adap_neoName;
            delete item._jsoq_adap_neoName
            for (let key of Object.keys(item)){
                attrs.push(`${key}: ${toCyperByType(item[key])}`)
            }
            return `(${_jsoq_adap_neoName}: ${collection} {${attrs.join(",")}})`
        }
        `
        MATCH ${match}
        CREATE ${Object.keys(items).map(col=>items[col].map(itm=>itemToCyper(itm, col)))}
        CREATE ${relations.map(relationToCyper)}, n->[:reverseRelation]->items
        `
    }
    getItemsAndRelations({collection, item, database, items, relations}){
        let targetCollection = database.getCollectionByName(collection);
        if (targetCollection===undefined){
            throw Error(`Collection '${collection}' not found.`)
        }
        else{
            targetCollection=targetCollection.model;
        }
        let associations = targetCollection.associations;
        let attributes = targetCollection.attributes;
        console.log(attributes, targetCollection)
        if (items===undefined){
            items = {};
        }
        if (items[collection]===undefined){
            items[collection] = [];
        }
        if (relations===undefined){
            relations = [];
        }
        let attrObj;
        if (item._jsoq_adap_neoName===undefined){
            attrObj = {_jsoq_adap_neoName: this.getNeoName(collection, items)};
        }
        else{
            attrObj = {_jsoq_adap_neoName: item._jsoq_adap_neoName}
        }
        for (let key of Object.keys(item)){
            if (attributes[key]){
                attrObj[key] = item[key];
            }
            else if (associations[key]){
                let associatedCollection = database.getCollectionByName(associations[key].collection);
                if (associatedCollection===undefined){
                    throw Error(`Collection '${associations[key].collection}' not found.`)
                }
                else{
                    associatedCollection=associatedCollection.model;
                }
                //generate new association
                let associationName = database.getAssociationIndexName(associatedCollection.name, key, associations[key]);
                relations.push({from: item._jsoq_adap_neoName, associationName, to: item[key]._jsoq_adap_neoName});
                this.getItemsAndRelations({collection: associatedCollection.name, item: item[key], database, items, relations});
            }
            else{
                throw Error(`'${key}' is not a recognized attribute or association of collection '${collection}'`);

            }
        }
        items[collection].push(attrObj);
        return {items, relations};
    }
    getNeoName(collection, items){
        let len = items[collection].length;
        return `${collection}${len}`;
    }
    /**
     * Updates a set of items
     * @param {*} lookup 
     * @param {*} item 
     */
    update(lookup, item){
        
    }
    /**
     * Deletes a set of items
     * @param {*} lookup 
     * @param {*} item 
     */
    delete(lookup, item){
        
    }

}

module.exports = CyperAdapter