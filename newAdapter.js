let AbstractAdapter = require("./abstractAdapter.js");
let implement = require("../implement")

/**
1 * matchDescent -
2 * matchDesire -
3 * transformDesire
4 * transformModifications
5 * setValuesAndRelations
6 * applyFilteringPipes
7 * applyAggregationPipes
8 * generateQuery
9 * createNodes
10 * runQuery
11 * runTransaction
 */

class CyperAdapter extends AbstractAdapter {
    constructor(settings){
        super();
        implement(settings, {
            host: "string",
            port: "number",
            user: "string",
            password: "string"
        })
        const neo4j = require('neo4j-driver').v1;
        const driver = neo4j.driver(`bolt://${settings.host}:${settings.port}`, neo4j.auth.basic(settings.user, settings.password));
        this.db = driver.session();
    }
    matchDescent({lookup, database}){
        //Since the lookup won't have fields if the lookup is for a insert or update, we can put write one general function for this
        return CyperHelper.createFullMatch({path: lookup.path, fields: lookup.fields, database});
    }
    matchDesire({lookup, database, matchedDescent}){
        return matchedDescent;
    }
    createNodes({lookup, database, items, matchedDescent}){
        let {items, relations} = CyperHelper.getItemsAndRelations()
        return {items, relations};
    }
    createRelations(nodes){
        return createdNodes;
    }
    setValuesAndRelations(items){
        
    }
    runQuery({matchClauses,whereClauses,createClauses,setClases,returnClause}){

    }
    applyFilteringPipes(){}
    applyAggregationPipes(){}
    generateQuery(){}
    runQuery(items, relations){}
    transformDesire(){}
    async ensureCollection(collection){
        let q = "";
        let attrs = collection.model.attributes;
        for (let attr of Object.keys(attrs)){
            q+=`CREATE INDEX ON :${collection.name}(attr)`
        }

    }
}

/** article[age>10].journal[name="somehting"]{name,papers{dateOfPublication}}
 * MATCH (article0:article)->[:article_journal]->(journal0:journal)
 * MATCH (journal0)->[:article_paper]->(paper0:paper)
 * 
 */

createFullMatch(path,fields,database){
    let matches = [];
    let conditions = []
    let returnItems = [];
    let whereClauses = [];
    let baseCollection = database.getCollectionByName(path[0].name);
    let attributes =  baseCollection.attributes;
    let associations = baseCollection.associations;
    let restPath = path.slice(1,Infinity);
    for (let item of restPath){
        if (associations[item.name]){
            let itemCollection = associations[item.name].collection;
            match+=`->[:${index}]->(${item.label}:${itemCollection})`;
            if (item.fields){
                for (let path of item.fields){
                    match+="\n"+createMatchClauseAndAttachLabelsToPath(itemCollection, database, path, baseItems);
                }
            }
        }
        else if (attributes[item.name]){
            throw Error(`Attribute '${item.name}' of '${collection}' is not an association`)
        }
        else{
            throw Error(`Attribute '${item.name}' of '${collection}' not found`)
        }
    }
    for (let field of fields){
        if (associations[item.name]){
            let itemCollection = associations[item.name].collection;
            match+=`->[:${index}]->(${item.label}:${itemCollection})`;
            if (item.fields){
                for (let path of item.fields){
                    match+="\n"+createMatchClauseAndAttachLabelsToPath(itemCollection, database, path, baseItems);
                }
            }
        }
        else if (attributes[item.name]){
            throw Error(`Attribute '${item.name}' of '${collection}' is not an association`)
        }
        else{
            throw Error(`Attribute '${item.name}' of '${collection}' not found`)
        }
    }
    for (let condition of conditions){
        whereClauses.push(`${condition.attribute} ${toCyperComparison(condition.comparison)} ${toCyperValue(condition.value)}`)
    }
    return {where: whereClauses.join(",")}
}
//where need references for each field in field
class CyperHelpers{
    static fieldsToPath(path, fields){
    
    }
    static createMatchClauseAndAttachLabelsToPath({path, database}){
        //All based on Path/Fields
        //

        let match = "";
        let baseCollection = database.getCollectionByName(collection);
        let attributes =  baseCollection.attributes;
        let associations = baseCollection.associations;
        for (let item of path){
            if (attributes[item.name]){
                //Just ignore if we're looking though Fields. Will be handled by createReturnClause
            }
            else if (associations[item.name]){
                let itemCollection = associations[item.name].collection;
                match+=`->[:${index}]->(${item.label}:${itemCollection})`;
                if (item.fields){
                    for (let path of item.fields){
                        match+="\n"+createMatchClauseAndAttachLabelsToPath(itemCollection, database, path, baseItems);
                    }
                }
            }
            else{
                throw Error(`Attribute '${item.name}' of '${collection}' not found`)
            }
        }
    }
    static createMatchClauseAndAttachLabelsToFields({fields, database}){
        //All based on Path/Fields
        // how the hell are we going to make this work
        // why wont these just have the 

        let match = "";
        let baseCollection = database.getCollectionByName(collection);
        let attributes =  baseCollection.attributes;
        let associations = baseCollection.associations;
        for (let item of path){
            if (attributes[item.name]){
                //Just ignore if we're looking though Fields. Will be handled by createReturnClause
            }
            else if (associations[item.name]){
                let itemCollection = associations[item.name].collection;
                match+=`->[:${index}]->(${item.label}:${itemCollection})`;
                if (item.paths){
                    for (let path of item.paths){
                        match+="\n"+createMatchClauseAndAttachLabelsToPath(itemCollection, database, path, baseItems);
                    }
                }
            }
            else{
                throw Error(`Attribute '${item.name}' of '${collection}' not found`)
            }
        }
    }
    static createWhereClauseFromPossiblyNestedPath(paths){
        //All based on Filters
    }
    static createReturnClause(paths){

    }
}

module.exports = CyperAdapter;